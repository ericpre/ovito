////////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2018 Alexander Stukowski
//
//  This file is part of OVITO (Open Visualization Tool).
//
//  OVITO is free software; you can redistribute it and/or modify it either under the
//  terms of the GNU General Public License version 3 as published by the Free Software
//  Foundation (the "GPL") or, at your option, under the terms of the MIT License.
//  If you do not alter this notice, a recipient may use your version of this
//  file under either the GPL or the MIT License.
//
//  You should have received a copy of the GPL along with this program in a
//  file LICENSE.GPL.txt.  You should have received a copy of the MIT License along
//  with this program in a file LICENSE.MIT.txt
//
//  This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
//  either express or implied. See the GPL or the MIT License for the specific language
//  governing rights and limitations.
//
////////////////////////////////////////////////////////////////////////////////////////

#pragma once


#include <ovito/stdobj/gui/StdObjGui.h>
#include <ovito/stdobj/series/DataSeriesObject.h>
#include <ovito/stdobj/gui/widgets/DataSeriesPlotWidget.h>
#include <ovito/stdobj/gui/properties/PropertyInspectionApplet.h>

namespace Ovito { namespace StdObj {

/**
 * \brief Data inspector page for 2d plots.
 */
class OVITO_STDOBJGUI_EXPORT SeriesInspectionApplet : public PropertyInspectionApplet
{
	Q_OBJECT
	OVITO_CLASS(SeriesInspectionApplet)
	Q_CLASSINFO("DisplayName", "Data Series");

public:

	/// Constructor.
	Q_INVOKABLE SeriesInspectionApplet() : PropertyInspectionApplet(DataSeriesObject::OOClass()) {}

	/// Returns the key value for this applet that is used for ordering the applet tabs.
	virtual int orderingKey() const override { return 200; }

	/// Lets the applet create the UI widget that is to be placed into the data inspector panel.
	virtual QWidget* createWidget(MainWindow* mainWindow) override;

	/// Returns the plotting widget.
	DataSeriesPlotWidget* plotWidget() const { return _plotWidget; }

protected:

	/// Creates the evaluator object for filter expressions.
	virtual std::unique_ptr<PropertyExpressionEvaluator> createExpressionEvaluator() override {
		return std::make_unique<PropertyExpressionEvaluator>();
	}

	/// Is called when the user selects a different property container object in the list.
	virtual void currentContainerChanged() override;

private Q_SLOTS:

	/// Action handler.
	void exportDataToFile();

private:

	/// The plotting widget.
	DataSeriesPlotWidget* _plotWidget;

	MainWindow* _mainWindow;
	QStackedWidget* _stackedWidget;
	QAction* _exportSeriesToFileAction;
};

}	// End of namespace
}	// End of namespace
